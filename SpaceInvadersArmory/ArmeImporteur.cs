﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpaceInvadersArmory
{
    public class ArmeImporteur
    {
        const int WEAPON_NAME_LENGTH_MIN = 3;
        List<string> WeaponsBlacklist = new List<string> {"Rayon lazer de la mort", "coucou", "salut"};
        public ArmeImporteur()
        {
        }

        public void AddWeaponsFromFile(string filePath)
        {
            Dictionary<string, int> newWeapons = new Dictionary<string, int>();

            string currentDirectory = Environment.CurrentDirectory;
            string fullPath = Path.Combine(currentDirectory, filePath);

            if(!File.Exists(fullPath))
            {
                throw new FileNotFoundException(fullPath);
            }

            // Read file and fill the dictionnary
            string line;
            StreamReader stream = new StreamReader(fullPath);
            try
            {
                while ((line = stream.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    int value;
                    //newWeapons.Add(line, newWeapons.TryGetValue(line, out value) ? value + 1 : 0);
                    newWeapons[line] = newWeapons.TryGetValue(line, out value) ? value + 1 : 1;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                stream.Close();
            }

            this.PrintDictionnary(newWeapons);
            
            // Create blueprints to armory
            Random random = new Random();
            foreach(KeyValuePair<string, int> kvp in newWeapons)
            {
                if(kvp.Key.Length < WEAPON_NAME_LENGTH_MIN) { continue; }
                if(WeaponsBlacklist.Contains(kvp.Key)) { continue; }

                Array WeaponTypes = Enum.GetValues(typeof(EWeaponType));
                Armory.CreatBlueprint(
                    kvp.Key,
                    (EWeaponType)WeaponTypes.GetValue(random.Next(WeaponTypes.Length)),
                    Math.Min(kvp.Value, kvp.Key.Length),
                    Math.Max(kvp.Value, kvp.Key.Length)
                );
            }

        }

        private void PrintDictionnary(Dictionary<string, int> newWeapons)
        {
            foreach (KeyValuePair<string, int> kvp in newWeapons)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }
        } 
    }
}
